
import '../data_state/data_state.dart';

Future<DataState<T>> handle<T>(Future<T> Function() callBack) async {
  try{
    var res = await callBack();
    return DataState(
      data: res,
      status: DataStatus.success
    );
  }
  catch(e){
    return DataState(
        status: DataStatus.error,
        message: e.toString()
    );
  }
}