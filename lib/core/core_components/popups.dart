
import 'package:flutter/material.dart';


Future<void> showLoader(BuildContext context) async =>await showDialog(
    context: context,
    builder: (_) =>WillPopScope(
      onWillPop: () async{
        return false;
      },
      child: const Center(
        child: CircularProgressIndicator(),
      ),
    )
);

Future<void> showSnackBar(BuildContext context,String content) async => ScaffoldMessenger.of(context)
    .showSnackBar(SnackBar(content: Text(content)));