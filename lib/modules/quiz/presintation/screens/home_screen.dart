import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quiz_admin/core/core_components/app_text_from_field.dart';
import 'package:quiz_admin/core/core_components/popups.dart';
import 'package:quiz_admin/core/handler/handler.dart';
import 'package:quiz_admin/modules/quiz/presintation/screens/section_screen.dart';
import 'package:sizer/sizer.dart';

import '../../../../core/core_components/status_component.dart';
import '../../data/data_source/quizzes_data_source.dart';
import '../controller/sections_cubit.dart';


class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  static const name = '/';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('sections'),
      ),
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.all(5.w),
        child: Column(
          children: [
            SizedBox(
              height: 10.w,
              child: const FittedBox(
                child: Text(
                  'All Sections',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Expanded(
                child: BlocBuilder<SectionsCubit, SectionsState>(
                  builder: (context, state) {
                    return StatusComponent(
                      status: state.dataState.status,
                      onSuccess: (context) => ListView.builder(
                        itemCount: state.dataState.data!.length,
                        itemBuilder: (context,i) => GestureDetector(
                          onTap: ()async{
                            await Navigator.push(context,
                              MaterialPageRoute(builder: (context) => SectionsScreen(state.dataState.data![i])),
                            );
                            Navigator.pushNamedAndRemoveUntil(context, name,(_)=>false);
                          },
                          onLongPress: ()=> showDialog(
                              context: context,
                              builder: (context) => Dialog(
                                child: ListTile(
                                  leading: Icon(Icons.delete),
                                  title: Text('delete'),
                                  onTap: ()async{
                                    showLoader(context);
                                    await handle(() => QuizzesDataSource.deleteSection(state.dataState.data![i]));
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    Navigator.pushNamedAndRemoveUntil(context, name,(_)=>false);
                                  },
                                ),
                              )
                          ),
                          child: Container(
                            width: double.infinity,
                            height: 15.w,
                            margin: EdgeInsets.all(5.w).copyWith(bottom: 1.w),
                            decoration: BoxDecoration(
                              color: Color(0xFF3CCFCF),
                              borderRadius: BorderRadius.circular(2.5.w),
                            ),
                            alignment: Alignment.center,
                            child: Text(
                              state.dataState.data![i].name,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.sp,
                                fontWeight: FontWeight.w700
                              ),
                            ),
                          ),
                        ),
                      ),
                      onError: (context) => Center(
                        child: Text(state.dataState.message),
                      ),
                    );
                  },
                )
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: (){
          TextEditingController nameController = TextEditingController();
          showDialog(context: context,
              builder: (context)=>Dialog(
                child: SizedBox(
                  height: 50.w,
                  width: 50.w,
                  child: Column(
                    children: [
                      AppTextFormField(
                        controller: nameController,
                      ),
                      ElevatedButton(
                          onPressed: ()async{
                            if(nameController.text.isEmpty) {
                              Navigator.pop(context);
                              showSnackBar(context, 'error');
                              return;
                            }
                            showLoader(context);
                            await handle(() => QuizzesDataSource.createSection(nameController.text));
                            Navigator.pop(context);
                            Navigator.pushNamedAndRemoveUntil(context, name,(_)=>false);
                          },
                          child: Text('Create')
                      )
                    ],
                  ),
                ),
              )
          );
        },
      ),
    );
  }
}
