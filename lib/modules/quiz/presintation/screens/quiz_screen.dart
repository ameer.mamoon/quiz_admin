import 'dart:math';

import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../../../core/core_components/app_text_from_field.dart';
import '../../../../core/core_components/popups.dart';
import '../../data/models/section_model.dart';

class QuizScreen extends StatefulWidget {
  final QuizModel model;

  QuizScreen(this.model, {Key? key}) : super(key: key);

  @override
  State<QuizScreen> createState() => _QuizScreenState();
}

class _QuizScreenState extends State<QuizScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.model.name),
      ),
      body: Column(
        children: [
          Expanded(
            flex: 8,
            child: ListView.builder(
                itemCount: widget.model.questions.length,
                itemBuilder: (context, i) =>
                    Container(
                      width: double.infinity,
                      height: 80.h,
                      padding: EdgeInsets.all(5.w),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.lightbulb,
                                size: 10.w,
                              ),
                              SizedBox(
                                width: 2.5.w,
                              ),
                              Text(
                                  'Question ${i + 1} of ${widget.model.questions.length}',
                                  style: TextStyle(
                                      fontSize: 24.sp,
                                      fontWeight: FontWeight.bold,
                                      color: Color(0xFF3CCFCF)
                                  )
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.w,
                          ),
                          Text(
                            widget.model.questions[i].question,
                            style: TextStyle(
                                fontSize: 22.sp,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                          SizedBox(
                            height: 5.w,
                          ),
                          Expanded(
                            child: ListView.builder(
                                physics: const PageScrollPhysics(),
                                itemCount: widget.model.questions[i].options.length,
                                itemBuilder: (context, j) =>
                                    Container(
                                      height: 20.w,
                                      margin: EdgeInsets.all(2.5.w),
                                      decoration: BoxDecoration(
                                          color: Color(0xFFFF5353),
                                          borderRadius: BorderRadius.circular(8.w)
                                      ),
                                      alignment: Alignment.center,
                                      child: Text(
                                        widget.model.questions[i].options[j],
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 20.sp,
                                            fontWeight: FontWeight.bold
                                        ),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    )
                            ),
                          )
                        ],
                      ),
                    )
            ),
          ),
          Expanded(
              child: Padding(
                padding: EdgeInsets.all(5.w),
                child: ElevatedButton(
                  onPressed: (){
                    Navigator.pop(context);
                  },
                  child: Text('done'),
                ),
              )
          )
        ],
      ),

      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: (){
          TextEditingController nameController = TextEditingController();
          TextEditingController ansController = TextEditingController();
          TextEditingController ans2Controller = TextEditingController();
          TextEditingController ans3Controller = TextEditingController();
          TextEditingController ans4Controller = TextEditingController();
          showDialog(context: context,
              builder: (context)=>Dialog(
                child: SingleChildScrollView(
                  child: SizedBox(
                    width: 50.w,
                    child: Column(
                      children: [
                        AppTextFormField(
                          hint: 'question',
                          controller: nameController,
                        ),
                        AppTextFormField(
                          hint: 'right answer',
                          controller: ansController,
                        ),
                        AppTextFormField(
                          hint: 'wrong answer',
                          controller: ans2Controller,
                        ),
                        AppTextFormField(
                          hint: 'wrong answer',
                          controller: ans3Controller,
                        ),
                        AppTextFormField(
                          hint: 'wrong answer',
                          controller: ans4Controller,
                        ),
                        ElevatedButton(
                            onPressed: ()async{
                              if(
                              nameController.text.isEmpty ||
                              ansController.text.isEmpty ||
                              ans2Controller.text.isEmpty ||
                              ans3Controller.text.isEmpty ||
                              ans4Controller.text.isEmpty
                              ) {
                                showSnackBar(context, 'error');
                                return;
                              }
                              widget.model.questions.add(QuestionModel(
                                  question: nameController.text,
                                  ans: ansController.text,
                                  options: [
                                    ansController.text,
                                    ans4Controller.text,
                                    ans2Controller.text,
                                    ans3Controller.text,
                                  ]..sort((o1,o2)=>Random().nextInt(5) - Random().nextInt(7)),
                              ));
                              setState(() {

                              });
                              Navigator.pop(context);
                            },
                            child: Text('Create')
                        )
                      ],
                    ),
                  ),
                ),
              )
          );
        },
      ),
    );
  }
}
