import 'package:flutter/material.dart';
import 'package:quiz_admin/core/core_components/popups.dart';
import 'package:quiz_admin/core/handler/handler.dart';
import 'package:quiz_admin/modules/quiz/presintation/screens/quiz_screen.dart';
import 'package:sizer/sizer.dart';

import '../../../../core/core_components/app_text_from_field.dart';
import '../../data/data_source/quizzes_data_source.dart';
import '../../data/models/section_model.dart';

class SectionsScreen extends StatefulWidget {
  final SectionModel model;
  const SectionsScreen(this.model,{Key? key}) : super(key: key);

  @override
  State<SectionsScreen> createState() => _SectionsScreenState();
}

class _SectionsScreenState extends State<SectionsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.model.name),
      ),
      body: Padding(
        padding: EdgeInsets.all(2.5.w),
        child: Column(
          children: [
            Expanded(
              child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: 2.5.w,
                  crossAxisSpacing: 2.5.w
                ),
                itemCount: widget.model.quizzes.length,
                itemBuilder: (context,i) => GestureDetector(
                  onTap: ()=> Navigator.push(context,
                    MaterialPageRoute(builder: (context) => QuizScreen(widget.model.quizzes[i])),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.blue[600]!
                      )
                    ),
                    child: Column(
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            '#${i+1}',
                            style: TextStyle(
                              color: Color(0xFFFF5353),
                              fontSize: 20.sp,
                              fontWeight: FontWeight.w900
                            ),
                          ),
                        ),
                        Text(widget.model.quizzes[i].name),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.all(2.5.w),
                child: ElevatedButton(
                  child: const Text('Update'),
                  onPressed: ()async{
                    showLoader(context);
                    var res = await handle(() => QuizzesDataSource.updateSection(widget.model));
                    if(res.message.isNotEmpty)
                      showSnackBar(context, res.message);
                    Navigator.pop(context);
                    Navigator.pop(context);
                  },
                ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: (){
          TextEditingController nameController = TextEditingController();
          showDialog(context: context,
              builder: (context)=>Dialog(
                child: SizedBox(
                  height: 50.w,
                  width: 50.w,
                  child: Column(
                    children: [
                      AppTextFormField(
                        controller: nameController,
                      ),
                      ElevatedButton(
                          onPressed: (){
                            if(nameController.text.isEmpty){
                              Navigator.pop(context);
                              showSnackBar(context, 'error');
                              return ;
                            }
                            widget.model.quizzes.add(QuizModel(
                                name: nameController.text,
                                questions: []
                            ));
                            setState(() {

                            });
                            Navigator.pop(context);
                          },
                          child: Text('Create')
                      )
                    ],
                  ),
                ),
              )
          );
        },
      ),
    );
  }
}
