
import 'package:cloud_firestore/cloud_firestore.dart';

import '../../../../core/storage/storage_handler.dart';
import '../models/section_model.dart';

class QuizzesDataSource{

  static final _fb = FirebaseFirestore.instance;

  static Future<List<SectionModel>> getSections()async{
    var collection = _fb.collection("secction");
    var result = await collection.get();
    return result.docs.map((e) => SectionModel.fromJson(e.data())).toList();
  }

  static Future<bool> createSection(String name)async{
    var collection = _fb.collection("secction");
    await collection.add({
      "name":name,
      "quizzes":[]
    });
    return true;
  }

  static Future<bool> updateSection(SectionModel section)async{
    var collection = _fb.collection("secction");
    var sec = await collection.where('name',isEqualTo: section.name).get();
    collection.doc(sec.docs.first.id).update(section.json);
    return true;
  }

  static Future<bool> deleteSection(SectionModel section)async{
    var collection = _fb.collection("secction");
    var sec = await collection.where('name',isEqualTo: section.name).get();
    await collection.doc(sec.docs.first.id).delete();
    return true;
  }





}